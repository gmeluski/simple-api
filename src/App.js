// @flow

import { Button, Container, OutlinedInput } from '@material-ui/core';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import './App.css';

type StackItem = {
  is_answered: Boolean,
  link: string,
  title: string,
}

const getSearchUrl = (searchTerm: string) => {
  const amendedSearch = `${searchTerm} not working`

  return `https://api.stackexchange.com/2.2/search?site=diy&sort=activity&order=desc&intitle=${amendedSearch}`;
}

type SearchProps = {
  onChange: (SyntheticEvent<HTMLInputElement>) => void,
  onClick: (Event) => Promise<void>,
}

const SearchOptions = ({ onChange, onClick }: SearchProps) => (
  <div className="search">
    <div>
      <form onSubmit={onClick}>
      My <OutlinedInput variant="" onChange={onChange} placeholder='lights' />
      {'\n'}are not working{'\n'}
      <Button variant="contained" color="primary" onClick={onClick}>Fix It!</Button>
      </form>
    </div>
  </div>
)

type ResultsProps = {
  results: Array<StackItem>
}

export const NoResults = () => (<div>No Results</div>)

export const Results = ({ results }: ResultsProps) => {
  if (results.length === 0) {
    return (<NoResults />);
  }

  return (
    <div className='results'>
      { results.map((result, index) =>
        (<QueryResult key={index} result={result} />)
      )}
    </div>
  )
}

const QueryResult = ({ result }: {result: StackItem}) => (
  <div><a target="_blank" href={result.link}>{result.title}</a></div>
)


function App() {
  const [results, setResults] = useState([])
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    const getStackResponse = async () => {
      const initialSearchTerm = 'lights';
      const items = await search(initialSearchTerm);
      setResults(items);
    }

    getStackResponse();
  }, [])

  const search = async (searchTerm: string) => {
    const searchUrl = getSearchUrl(searchTerm)
    const response = await axios.get(searchUrl)
    const { items } = response.data;
    return items;
  }

  const onChange = (event) => {
    const { value } = event.currentTarget
    setSearchTerm(value);
  }

  const onClick = async (event) => {
    event.preventDefault();
    const items = await search(searchTerm);
    setResults(items);
  }

  return (
    <Container>
      <h1>What's Not Working</h1>
      <h3>Tell us what's not working, maybe we can help</h3>
      <SearchOptions onChange={onChange} onClick={onClick} />
      <Results results={results} />
    </Container>
  );
}

export default App;
