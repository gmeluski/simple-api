import React from 'react';
import ReactDOM from 'react-dom';
import App, { NoResults, Results } from './App';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

const setup = (extraProps = {}) => {
  const props = {
    ...extraProps,
  }

  const results = renderer.create(<Results {...props} />)

  return {
    results
  };
}

describe('Results', () => {
  it('returns no results when the results length is 0', () => {
    const props = {
      results: [],
    }
    const {results} = setup(props);

    expect(() => {
      results.root.findByType(NoResults);
    }).not.toThrow();
  })
})
