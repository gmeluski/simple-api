# Quickstart

- Clone this repo to a local directory
- install dependencies: `yarn`
- `yarn start`

# Tools

This is a proof of concept to use the DIY Stack Overflow API to return answers for my most common searches, which are "[thing here] isn't working"

In the interest of speed I used a couple tools. 
- Create React App - this creates some nice scaffolding for the web app and a handy dev environment. The hot reloading helps out tons when it comes to iterating early development.
- Flow - I like the safety of types. What can I say.
- Jest - This comes packaged with Create React App. I find it handy for doing TDD-like development
- Material UI - Mainly for form elements. IMO these always need a little visual softening and can be time-consuming to create manually.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
